var fs = require('fs');

var input = [];

// function to read the input from a given file in command line
function readInput() {
    var fileName = process.argv[2];
    fs.readFile(fileName, 'utf8', function(err, data) {
        if (err) console.log('Error in reading file ' + err);

        // copy the input over to an array
        input = Array.from(data.split('\n'));

        calculateDrivingHistory();
    });
}

readInput();

// calculate driving history based on the input values
function calculateDrivingHistory() {
    var driverTrip = {};

    input.sort( function(x,y) { x-y });

    for( var i=0; i<input.length; i++) {

        // if the current element starts with 'Driver' add it to driverTrip object as key
        if(String(input[i]).startsWith('Driver')) {
            var name = input[i].split(' ')[1];
            driverTrip[name] = [];
        } else {
            // current element is a 'Trip' element, retrieve time and miles value
            var trip = String(input[i]).split(' ');

            // calculate start time and end time in ms
            var startTime = new Date();
            startTime.setHours(trip[2].split(':')[0], trip[2].split(':')[1]);
            var endTime = new Date();
            endTime.setHours(trip[3].split(':')[0], trip[3].split(':')[1]);

            // calculate time taken and speed for the current trip
            var miles = Number(trip[4]);
            var time = (endTime - startTime)/60000;
            var speed = ((miles * 60)/time).toFixed(0);

            // if the speed is less than 5 or greater than 100, break out of loop
            if(speed < 5 || speed > 100) {
                break;
            }

            // if the driverTrip objet already contains the key 'driver name', then update the
            // miles, time and speed values for that driver
            if(driverTrip.hasOwnProperty(trip[1]) && driverTrip[trip[1]].length !== 0) {
                var addedMiles = (Number(driverTrip[trip[1]][0]) + miles).toFixed(0);
                driverTrip[trip[1]][0] = addedMiles;

                var addedTime = (Number(driverTrip[trip[1]][1]) + time).toFixed(0);
                driverTrip[trip[1]][1] = addedTime;

                var newSpeed = ((addedMiles * 60)/addedTime).toFixed(0);
                driverTrip[trip[1]][2] = newSpeed;
            } else {
                // driver doesnt exists in object, add new object for the driver
                driverTrip[trip[1]] =[miles, time, speed];
            }
        }
    }
    printOutput(driverTrip);
}

// function to print output from a given object
function printOutput(driverTrip) {
    var output = [];

    // copy content from object over to an aray and sort by miles driven
    for(var key in driverTrip) {
        output.push([key, driverTrip[key][0]]);
    }

    output.sort( function(x,y) {
        return y[1] - x[1];
    });

    // iterate through the sorted output array and print each drivers history from driverTrip object
    for(var i=0; i<output.length; i++) {
        var key = output[i][0];

        if(driverTrip[key][0] !== undefined) {
            console.log(key + ': ' + driverTrip[key][0] + ' miles @ ' + driverTrip[key][2] + ' mph');
        } else {
            console.log(key + ': 0 miles');
        }
    }
}